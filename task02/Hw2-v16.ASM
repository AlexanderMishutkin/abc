
; ������� 15: ������ B �� ��������� ������ �������� ���������������

format PE console
entry start

include 'win32a.inc'

;--------------------------------------------------------------------------
section '.data' data readable writable

        strVecSize     db 'size of vector: ', 0
        strIncorSize   db 'Incorrect size of vector = %d', 10, 13,  0
        strVecElemI    db '[%d]? ', 0
        strScanInt     db '%d', 0
        strMaxValue    db 'Max = %d', 10, 13,  0
        strBVec        db '---B---', 10, 13,  0
        strVecElemOut  db '[%d] = %d', 10, 13,  0

        res db 'Average val: %d',0
        res3 db '/%d',10, 13, 0
        itpt db ' and %d',0

        numerator    dd 0
        vec_size     dd 0
        sum          dd 0
        i            dd 0
        i_b            dd 0
        tmp          dd ?
        tmp_b          dd ?
        tmpStack     dd ?
        vec          rd 100
        vecB          rd 100

;--------------------------------------------------------------------------
section '.code' code readable executable
start:
; 1) vector input
        call VectorInput
; 2) get vector max
;        call InitLoop ; ���������� � ������� �� �������
;        call VectorAvgLoop ; ���� ������� � �����
; 3) replacing zeros
        call InitLoop ; ���������� � ������� �� �������
        call SelectVecB ; �������� ���� �����������
; 4)
; 4) out of avg
        call PrintAvg
; 5) out of B Vector
        push strBVec
        call [printf]

; 6) test vector out
        call VectorOut
finish:
        call [getch]

        push 0
        call [ExitProcess]

;--------------------------------------------------------------------------
VectorInput:
        push strVecSize
        call [printf]
        add esp, 4

        push vec_size
        push strScanInt
        call [scanf]
        add esp, 8

        mov eax, [vec_size]
        cmp eax, 0
        jg  getVector

        push [vec_size]
        push strIncorSize
        call [printf]
        call [getch]
        push 0
        call [ExitProcess]
;---------------------------------------------------------------------------
getVector:
        xor ecx, ecx
        mov ebx, vec
getVecLoop:
        mov [tmp], ebx
        cmp ecx, [vec_size]
        jge endInputVector

        ; input element
        mov [i], ecx
        push ecx
        push strVecElemI
        call [printf]
        add esp, 8

        push ebx
        push strScanInt
        call [scanf]
        add esp, 8

        ; cnt sum
        mov eax, [sum]
        add eax, [ebx]
        mov [sum], eax

        mov ecx, [i]
        inc ecx
        mov ebx, [tmp]
        add ebx, 4
        jmp getVecLoop
endInputVector:
        mov ebx, vec
        xor ecx, ecx
        ret
;--------------------------------------------------------------------------
InitLoop:
        xor ecx, ecx
        mov ebx, vec

        mov eax, vecB
        mov [tmp_b], eax

        ret
;--------------------------------------------------------------------------
SelectVecB:
        mov [tmp], ebx
        cmp ecx, [vec_size]
        jge endBSelect
        mov [i], ecx ; ����� �� �����

        mov eax, [ebx]
        mul [vec_size]
        cmp eax, [sum] ; ���� ������� ��-� ������ ��������
        jg more

        middleOfLoop:
        mov ecx, [i]
        inc ecx
        mov ebx, [tmp]
        add ebx, 4

        jmp SelectVecB

more:
        mov eax, [tmp_b]
        mov ecx, [ebx]
        mov [eax], ecx

        mov ecx, [i_b]
        inc ecx
        mov [i_b], ecx

        mov eax, [tmp_b]
        add eax, 4
        mov [tmp_b], eax

        jmp middleOfLoop

endBSelect:
        ret
;--------------------------------------------------------------------------
PrintAvg:
        mov eax, [sum]
        mov ecx, [vec_size]
        mov edx, 0

        div ecx
        mov [numerator], edx
        cinvoke printf, res, eax
        cinvoke printf, itpt, [numerator]
        cinvoke printf, res3, [vec_size]

        ret
;--------------------------------------------------------------------------
VectorOut:
        mov [tmpStack], esp
        xor ecx, ecx            ; ecx = 0
        mov ebx, vecB            ; ebx = &vec
putVecLoop:
        mov [tmp], ebx
        cmp ecx, [i_b]
        je endOutputVector      ; to end of loop
        mov [i], ecx

        ; output element
        push dword [ebx]
        push ecx
        push strVecElemOut
        call [printf]

        mov ecx, [i]
        inc ecx
        mov ebx, [tmp]
        add ebx, 4
        jmp putVecLoop
endOutputVector:
        mov esp, [tmpStack]
        ret
;-------------------------------third act - including HeapApi--------------------------
                                                 
section '.idata' import data readable
    library kernel, 'kernel32.dll',\
            msvcrt, 'msvcrt.dll',\
            user32,'USER32.DLL'

include 'api\user32.inc'
include 'api\kernel32.inc'
    import kernel,\
           ExitProcess, 'ExitProcess',\
           HeapCreate,'HeapCreate',\
           HeapAlloc,'HeapAlloc'
  include 'api\kernel32.inc'
    import msvcrt,\
           printf, 'printf',\
           scanf, 'scanf',\
           getch, '_getch'