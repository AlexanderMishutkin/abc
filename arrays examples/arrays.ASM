format PE console
entry start

include 'win32a.inc'
section '.data' data readable writable

        enterN db 'Enter N:', 10, 13, '>> ', 0
        endl db 10, 13, 0
        enterStart db 'Enter the ', 0
        enterEnd db '-st elemet', 10, 13, '>> ', 0
        outStart db '[ ', 0
        outEnd db ' ]', 10, 13, 0
        outMiddle db ', ', 0

        hheap dd ?
        mas dd ?

        i dd ?

        null dd 0

        digit db '%d', 0

        N dd ?
                
section '.code' code readable executable
        start:
;-------------------------------first act - creating array--------------------------
                push enterN
                call [printf]

                push N
                push digit
                call [scanf]

                ;creating heap API object
                invoke HeapCreate,HEAP_NO_SERIALIZE,0,0
                mov [hheap],eax

                ;booking some memory in heap,
                ;N*8 - amout of memory we book
                ;in eax would be address of first reserved element
                invoke HeapAlloc,[hheap],HEAP_NO_SERIALIZE,N*8

                ;If I write this code
                ;--> sub eax, 1000
                ;I would imideately catch exception,
                ;cause memmory before [mas] isn't allocated
                ;this fact proves that HeapAlloc works

                mov [mas],eax

                ;You can not move '3' in array cell
                ;You need to format 3 to 4-byte format (0000 0003) with 'dword 3'
                ;Or you can put it in the register, and it will format 3 to 4-byte form automatically
                mov ebx, 3

                ;Let's put some data in array
                mov [eax+0*8], ebx
                mov [eax+1*8], ebx
                mov [eax+2*8], ebx
                mov [eax+3*8], ebx

                ;Let's grab some data from array
                ;We need to parse number in [eax+0*8] to 4-byte format, by writing dword
                push dword [eax+0*8]
                push digit
                call [printf]
                mov eax, [mas]

                ;Or you can just put [eax+0*8] in register, and push that register
                ;Then data would be formated automatically (as it is done in the second variant)
                mov ecx, [eax+1*8]
                push ecx
                push digit
                call [printf]
                mov eax, [mas]


                ;Or use cinvoke and do not care about parsing data (as it is done in the third variant)
                cinvoke printf, digit, [eax+2*8]
                mov eax, [mas]

                ;but you must ALWAYS SAVE REGISTER DATA in stack or better in heap before call or cinvoke

                push endl
                call [printf]

;-------------------------------second act - making loops--------------------------
;Classical loop example
;It's important to take ecx from stack
;Cause printf use registers
                mov [i], -1
                jmp loop1
loop1:
                add [i], 1 ;for (int i = 0; i < N; i++)

                cinvoke printf,digit, [i]
                ;It's loop's body
                ;Here correct counter is in heap ([i])

                mov ecx, [i] ;same as i < N
                add ecx, 1
                cmp ecx, [N]
                jne loop1
                jmp end1
end1:

                ;cinvoke is better then call)))
                ;there are bugs with double %d outputs
                cinvoke printf, endl


;That's loop for reading array
                mov [i], -1
                jmp loop2
loop2:
                add [i], 1

                cinvoke printf,enterStart
                cinvoke printf,digit,[i]
                cinvoke printf,enterEnd

;Here we count current array cell address
;It's mas + i * 4
;but we cannot operate with mas and i, so we use registers
                mov ecx, [i]
                imul ecx, dword 4
                mov eax, [mas]
                add eax, ecx
;now cell address is in eax
                cinvoke scanf, digit, eax


                mov ecx, [i]
                add ecx, 1
                cmp ecx, [N]
                jne loop2
                jmp end2
end2:


;That's loop for outputting array, it works like previos
;but there is small tape for nice output
                cinvoke printf,outStart
                mov [i], -1
                jmp loop3
loop3:
                add [i], 1

                mov ecx, [i]
                imul ecx, dword 4
                mov eax, [mas]
                add eax, ecx

                cinvoke printf, digit, dword [eax]

                mov ecx, [i]
                add ecx, 1

                cmp ecx, [N] ; If (i == n - 1)
                    jne printDot
                    jmp loop3cont
                printDot:    ; then printf ', '
                    cinvoke printf,outMiddle
                loop3cont:

                mov ecx, [i]
                add ecx, 1

                cmp ecx, [N]
                jne loop3
                jmp end3
end3:

                cinvoke printf,outEnd


                call [getch]

                push [null]
                call [ExitProcess]


                                

;-------------------------------third act - including HeapApi--------------------------
                                                 
section '.idata' import data readable
    library kernel, 'kernel32.dll',\
            msvcrt, 'msvcrt.dll',\
            user32,'USER32.DLL'

include 'api\user32.inc'
include 'api\kernel32.inc'
    import kernel,\
           ExitProcess, 'ExitProcess',\
           HeapCreate,'HeapCreate',\
           HeapAlloc,'HeapAlloc'
  include 'api\kernel32.inc'
    import msvcrt,\
           printf, 'printf',\
           scanf, 'scanf',\
           getch, '_getch'