format PE console
entry start
include 'win32a.inc'
section '.data' data readable writable
        topText db '/-------------------------------------\', 10, 13, 0
        cow db 10, 13, '\-------------------------------------/', 10, 13, \
'         \   ^__^', 10, 13, \
'          \  (oo)\_______', 10, 13, \
'             (__)\       )\/\', 10, 13, \
'                 ||----w |', 10, 13, \
'                 ||     ||', 10, 13, 0
        text db ?
        firstQuestion db 'Enter any sentence: ', 10, 13, 0
        secondQuestion db '>> ', 0
        length dd ?
        NULL = 0
                
section '.code' code readable executable
        start:
                push firstQuestion
                call [printf]
                push secondQuestion
                call [printf]
                                
                push text
                call [gets]

                push topText
                call [printf]
                push text
                call [printf]
                push cow
                call [printf]

                call [getch]
                push NULL
                call [ExitProcess]
                                
section '.idata' import data readable
    library kernel, 'kernel32.dll',\
            msvcrt, 'msvcrt.dll'
    import kernel,\
           ExitProcess, 'ExitProcess', \
           lstrlen, 'lstrlen'
    import msvcrt,\
           printf, 'printf',\
           scanf, 'scanf',\
           gets, 'gets',\
           getch, '_getch'

                   